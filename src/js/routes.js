import HomePage from '../pages/home.vue';
import Login from '@/pages/login.vue';
import AddNews from '@/pages/addnews.vue';
import NotFoundPage from '../pages/404.vue';
import NewsDetail from '../pages/newsdetail.vue';
import NewsDetail2 from '../pages/news_detail.vue';
import CatDetail from '../pages/catdetail.vue';
var routes = [{
        path: '/home',
        component: HomePage,
    },
    {
        path: '/',
        component: Login,
    },
    {
        path: '/a',
        component: AddNews,
    },
    {
        path: '/newsdetail/:id',
        component: NewsDetail,
    },
    {
        path: '/catdetail/:id',
        component: CatDetail,
       
    },
    {
        path: '(.*)',
        component: NotFoundPage,
    },
    
];

export default routes;